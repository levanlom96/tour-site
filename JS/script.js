$(document).ready(function () {
	var leftButton = $("#galleryLeft");
	var rightButton = $("#galleryRight");
	var movingContainer = $("#movingContainer");
	var anitmationIsGoing = false;
	var children = movingContainer.children();
	var childrenWidth = 0;


	leftButton.on("click", function(){
		var condition = !anitmationIsGoing;
		var leftPosition = movingContainer.css("left");
		var leftPosition = parseToNumber(leftPosition);

		childrenWidth = 0;
		myForeach(children, function(arrayItem){
			childrenWidth += arrayItem.offsetWidth;
		});
		
		condition = condition && (leftPosition < childrenWidth - 300);

		if(condition){
			anitmationIsGoing = true;
			movingContainer.animate({
				left: "-=300"
			}, 500 , function(){
				anitmationIsGoing = false;
			});
		}
	});

	rightButton.on("click", function(){
		var condition = movingContainer.css("left") != "0px" && !anitmationIsGoing;

		if(condition){
			anitmationIsGoing = true;			
			movingContainer.animate({
				left: "+=300"
			}, 500, function(){
				anitmationIsGoing = false;				
			});
		}
	});

	function myForeach(array, func){
		for(var i = 0; i < array.length; i++){
			func(array[i]);
		}
	}

	function parseToNumber(stringWithPixels){
		return Math.abs(Number(stringWithPixels.replace("px", "")));
	}

});